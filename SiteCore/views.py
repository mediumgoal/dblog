from django.shortcuts import render, HttpResponse
from .models import SiteSettings
from article.models import Article
def index(request):
    sitesettings = SiteSettings.objects.all()
    articles = Article.objects.all()
    return render(request, "index.html",{"settings":sitesettings,"articles":articles})
