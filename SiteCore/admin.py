from django.contrib import admin
from .models import SiteSettings
# Register your models here.
admin.site.register(SiteSettings)
class SiteAdmin(admin.ModelAdmin):
    pass
