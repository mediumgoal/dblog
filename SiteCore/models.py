from django.db import models


class SiteSettings(models.Model):
    site_title = models.CharField(max_length = 100, blank=True, null=True, verbose_name="Site title")
    description = models.CharField(max_length = 500,blank=True, null=True, verbose_name="Site açıklaması")
    logo = models.ImageField(blank=True, null=True, verbose_name="Logo")
    phone = models.CharField(max_length=30, blank=True, null=True, verbose_name="İletişim telefon")
    contact_email = models.EmailField(blank=True, null=True, verbose_name="İletişim e-posta")
    footer_text = models.CharField(max_length = 100, blank=True, null=True, verbose_name="Footer yazısı")
    cookie_info_text = models.TextField(blank=True, null=True, verbose_name="Cookie bilgilendirme yazısı")

    def __str__(self):
        return "Site ayarları"