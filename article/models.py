from django.db import models

class Article(models.Model):
    author = models.ForeignKey("auth.User",on_delete = models.CASCADE)
    title = models.CharField(max_length = 50, verbose_name = "Başlık")
    content = models.TextField(verbose_name = "İçerik")
    created_date = models.DateTimeField(auto_now= True, verbose_name="Tarih")
    thumb_image = models.ImageField(blank=True, null=True, verbose_name= "Thumbnail")

    def __str__(self):
        return "Makale"
    