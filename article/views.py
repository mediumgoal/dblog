from django.shortcuts import render, HttpResponse
from .models import Article
def index(request):
    articles = Article.objects.all()
    return render(request,"articles.html",{"articles":articles})
def detail(request, id):
    article = Article.objects.filter(id = id)
    return render(request,"detail.html",{"article":article})
